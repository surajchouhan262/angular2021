import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-event-bind',
  templateUrl: './event-bind.component.html',
  styleUrls: ['./event-bind.component.scss']
})
export class EventBindComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  msg:string='';
  addToCard(event) {
    this.msg=event.target.value + " Product added in card"
  }
  myInputClick(event){
    console.log(event.target.value);
    
  }

  infoInput(inoutInfo) {
    console.log(inoutInfo.name)
  }
}
