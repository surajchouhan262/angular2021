import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ngfor',
  templateUrl: './ngfor.component.html',
  styleUrls: ['./ngfor.component.scss']
})
export class NgforComponent implements OnInit {
  productData = [
    { proId: '01', proname: 'Mobile', proPrice: '15000' },
    { proId: '02', proname: 'TV', proPrice: '12000' },
    { proId: '03', proname: 'AC', proPrice: '32000' },
    { proId: '04', proname: 'Iphone', proPrice: '82000' }
  ]

  users: any[] = [];
  validation: any = {isNameNotValid: false, isEmailNotValid: false};
  
  constructor(private router:Router ) { }

  ngOnInit(): void {
  }

 


  createUser(uname, uemail) {
    this.validation.isNameNotValid = true;
    this.validation.isEmailNotValid = true;
    if (uname.value != '' && uemail.value != '') {
      console.log(uname.value)
      this.users.push({
        name: uname.value,
        email: uemail.value
      });
      this.validation.isNameNotValid = false;
      this.validation.isEmailNotValid = false;
      if (this.users.length > 3) {
        alert('hii');
        this.router.navigate(['login'])
      }
    } else {
      if (uname.value != '') {
        console.log(this.validation.isNameNotValid)
        this.validation.isNameNotValid = false;
      }
      else if (uemail.value != '') {
        console.log(this.validation.isEmailNotValid)
        this.validation.isEmailNotValid = false;
      } else {
        this.validation.isNameNotValid = true;
        this.validation.isEmailNotValid = true;
      }
    }


    
  }

  removeUser() {
    this.users.splice(this.users.length - 1)
  }
  removeItem(item) {
    this.users.splice(item, 1)
  }

}
