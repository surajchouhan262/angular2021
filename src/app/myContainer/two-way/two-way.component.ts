import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-two-way',
  templateUrl: './two-way.component.html',
  styleUrls: ['./two-way.component.scss']
})
export class TwoWayComponent implements OnInit {
  inoutVal: string = 'Suraj';

  slides: { image: string }[] = [];
  activeSlideIndex = 0;
  constructor() { 
    for (let i = 0; i < 8; i++) {
      this.addSlide();
    }
   }

  ngOnInit() {
  }
  

  addSlide(): void {
    this.slides.push({
      image: `assets/images/${this.slides.length % 8 + 1}.jpg`
    });
  }

}
