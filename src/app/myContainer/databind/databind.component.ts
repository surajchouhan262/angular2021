import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-databind',
  templateUrl: './databind.component.html',
  styleUrls: ['./databind.component.scss']
})
export class DatabindComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  Name:string='Suraj';

  myMethode() {
    return "My Name is " + this.Name
  }
  status1:string = 'Online';
  status2:string = 'Offline';
  
  enebled:boolean = true;

  appStatus:boolean = false
}
