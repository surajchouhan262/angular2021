import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-class-style',
  templateUrl: './class-style.component.html',
  styleUrls: ['./class-style.component.scss']
})
export class ClassStyleComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  mltClasses = {
    class1: true,
    class2:true,
    class3: true,
  }

  mltStyle = {
    'background-color':'red',
    'border': '5px solid #1f30ff'
  }

}
