import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MyContainer } from './myContainer/myContainer.component';
import { HeaderComponent } from './myContainer/header/header.component';
import { TopNavComponent } from './myContainer/top-nav/top-nav.component';
import { Txtsec1Component } from './myContainer/txtsec1/txtsec1.component';
import { DatabindComponent } from './myContainer/databind/databind.component';
import { ClassStyleComponent } from './myContainer/class-style/class-style.component';
import { EventBindComponent } from './myContainer/event-bind/event-bind.component';
import { TwoWayComponent } from './myContainer/two-way/two-way.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { NgifComponent } from './myContainer/ngif/ngif.component';
import { NgswitchComponent } from './myContainer/ngswitch/ngswitch.component';
import { NgforComponent } from './myContainer/ngfor/ngfor.component';
import { HomeComponent } from './home/home.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ServicesComponent } from './services/services.component';
import { ContactusComponent } from './contactus/contactus.component';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { Service1Component } from './services/service1/service1.component';
import { Service2Component } from './services/service2/service2.component';
import { Service3Component } from './services/service3/service3.component';
import { Service4Component } from './services/service4/service4.component';

const appRoute: Routes= [
  { path: "", redirectTo:'login', pathMatch:'full' },
  { path: "home", component: HomeComponent },
  { path: "login", component: LoginComponent },
  { path: "about", component: AboutusComponent },
  { path: "contact", component: ContactusComponent },
  {
    path: "services", component: ServicesComponent, children:[
    // { path: "", component: ServicesComponent },
    { path: "service1", component: Service1Component },
    { path: "service2", component: Service2Component },
    { path: "service3", component: Service3Component },
    { path: "service4", component: Service4Component },

  ] },
  { path: "**", component: PageNotFoundComponent }
]
// const routes: Routes = [{ path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule) }];

@NgModule({
  declarations: [
    AppComponent,
    MyContainer,
    HeaderComponent,
    TopNavComponent,
    Txtsec1Component,
    DatabindComponent,
    ClassStyleComponent,
    EventBindComponent,
    TwoWayComponent,
    NgifComponent,
    NgswitchComponent,
    NgforComponent,
    HomeComponent,
    AboutusComponent,
    ServicesComponent,
    ContactusComponent,
    LoginComponent,
    PageNotFoundComponent,
    Service1Component,
    Service2Component,
    Service3Component,
    Service4Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    CarouselModule.forRoot(),
    RouterModule.forRoot(appRoute)
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
